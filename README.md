#Atividade -3 

Versão 1.0.0
- Calculadora inicia utilizando spring boot mvc com java e html/css/js
- Calculadora com as operações básicas de soma e subtração

Versão 1.1.0
- Adição das funcionalidades de multiplicação e divisão

Versão 1.1.1
- Correção das casas decimais dos resultados

Versão 2.0.0
- Mudança de backend de java e spring boot para javascript e melhoria no layout da calculadora. 

Versão 2.1.0
- Acréscimo da logo da empresa na calculadora e adição de novas operações para calculadora. 

Versão 2.1.1
- Correção da função de multiplicação da calculadora e exibição dos resultados em até 2 casas decimais.

Versão 2.2.0
- Adição de modo escuro ao cricar na logo da empresa.
